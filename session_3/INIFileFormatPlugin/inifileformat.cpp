#include "inifileformat.h"

#include <QSettings>

INIFileFormat::INIFileFormat(QObject *parent) : QObject(parent)
{

}

bool INIFileFormat::saveAddressBook(const AddressBook *addressBook, const QString &fileName) const
{
    // Create a settings object to write INI files:
    QSettings ini( fileName, QSettings::IniFormat );

    // Start writing the list of address book entries:
    ini.beginWriteArray( "addressBook" );
    for ( int i = 0; i < addressBook->entries().length(); ++i ) {
        // Update the current element to write:
        ini.setArrayIndex( i );

        // Get the next entry from the address book:
        auto entry = addressBook->entries().at( i );

        // Begin a sub-group for the entries properties:
        ini.beginGroup( "addressBookEntry" );

        // Store the properties of the entry:
        ini.setValue( "name", entry->name() );
        ini.setValue( "birthday", entry->birthday() );
        ini.setValue( "address", entry->address() );
        ini.setValue( "phoneNumbers", entry->phoneNumbers() );

        // Finalize the group:
        ini.endGroup();
    }
    // Finalize the array:
    ini.endArray();

    // Done:
    return true;
}

bool INIFileFormat::loadAddressBook(AddressBook *addressBook, const QString &fileName) const
{
    // Create a settings object to read INIL
    QSettings ini( fileName, QSettings::IniFormat );

    // Start reading the list of entries in the file (returns the length of the array):
    int length = ini.beginReadArray( "addressBook" );
    for ( int i = 0; i < length; ++i ) {
        // Update the current entry:
        ini.setArrayIndex( i );

        // Start reading the group:
        ini.beginGroup( "addressBookEntry" );

        // Create a new entry:
        auto entry = addressBook->createEntry();
        if ( entry ) {
            // Read the values:
            entry->setName( ini.value( "name", QString() ).toString() );
            entry->setBirthday( ini.value( "birthday", QDate() ).toDate() );
            entry->setAddress( ini.value( "address", QString() ).toString() );
            entry->setPhoneNumbers( ini.value( "phoneNumbers", QStringList() ).toStringList() );
        }
        // Finalize the group:
        ini.endGroup();
    }
    // Finalize the arraz:
    ini.endArray();

    // Done:
    return true;
}

