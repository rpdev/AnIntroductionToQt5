# For XML support, we require Qt's XML module:
QT       += xml

QT       -= gui

TARGET = XMLFileFormatPlugin
TEMPLATE = lib
CONFIG += plugin c++11

SOURCES += xmlfileformatplugin.cpp \
    xmlfileformat.cpp

HEADERS += xmlfileformatplugin.h \
    xmlfileformat.h

# Put the resulting library in the AddressBookPlugins directory:
DESTDIR = ../bin/AddressBookPlugins

# Set up library search paths and link against AddressBookLibrary:
LIBS += -L../bin  -lAddressBookLibrary

# Include headers from the AddressBook library (lib dir):
INCLUDEPATH += ../lib
