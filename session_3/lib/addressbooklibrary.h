#ifndef ADDRESSBOOKLIBRARY_H
#define ADDRESSBOOKLIBRARY_H

#include <QtGlobal>

// If we are building the library itself...
#ifdef ADDRESS_BOOK_LIBRARY
// Define our export directive to be Q_DECL_EXPORT, causing symbols to be exported from a library...
#define ADDRESSBOOKLIBRARY_EXPORT Q_DECL_EXPORT
#else
// ... otherwise, define it to import symbols (i.e. we are building an executable/plugin
// which is built against the library):
#define ADDRESSBOOKLIBRARY_EXPORT Q_DECL_IMPORT
#endif

#endif // ADDRESSBOOKLIBRARY_H

