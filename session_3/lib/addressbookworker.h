#ifndef ADDRESSBOOKWORKER_H
#define ADDRESSBOOKWORKER_H

#include "addressbook.h"
#include "addressbookfile.h"

#include <QObject>

/**
 * @brief The AddressBookWorker class
 *
 * This class processes load and save requests of address books. It provides two slots
 * (saveAddressBook() and loadAddressBook()) which can be used to save and load address books
 * respectively. The addressBookSaved() and addressBookLoaded() signals are emitted to indicate
 * that a save or load operation finished. To have non-blocking save/load operations, an instance
 * of this class can be send to a QThread and it's slots been called via queued connections.
 */
class AddressBookWorker : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructor
     */
    explicit AddressBookWorker(QObject *parent = 0);

signals:

    /**
     * @brief An address book has been saved
     *
     * This signal is emitted, after the @p addressBook has been saved to a file. The @p success
     * argument indicated whether the saving was successful (it reflects the return value
     * of AddressBookFile::saveAddressBook()). As the AddressBookWorker does not own the addressBook,
     * this signal can be used delete it after the operation completed.
     */
    void addressBookSaved( const AddressBook *addressBook, bool success );

    /**
     * @brief An address book has been loaded
     *
     * This signal is emitted, after the @p addressBook has been loaded from a file. The
     * @p success argument indicates whether the loading was successful (it reflects the return
     * value of AddressBookFile::loadAddressBook()). As the AddressBookWorker does not own the
     * addressBook, this signal can be used to delete it after the operation completed.
     */
    void addressBookLoaded( AddressBook *addressBook, bool success );

public slots:

    /**
     * @brief Saves an address book to a file
     *
     * This method will save the @p addressBook to the @p fileName using the @p file object.
     * After the operation completed, the addressBookSaved() signal will be emitted.
     *
     * @note The worker will take ownership of the @p file and destroy it after the operation.
     * However, the worker does not assume ownership over the addressBook. Connect to the
     * addressBookSaved() signal and destroy the addressBook there if it is no longer required
     * after the operation.
     */
    void saveAddressBook( const AddressBook *addressBook, AddressBookFile *file, const QString &fileName );

    /**
     * @brief Loads an address book from a file
     *
     * This will load the @p addressBook from the @p fileName using the @p file object.
     * After the operation completed, the addressBookLoaded() signal will be emitted.
     *
     * @note The worker will take ownership of the @p file object and destroy it after the operation
     * completes. However, no ownershop over the addressBook is taken. If the addressBook is no
     * longer required after the operation, one should connect to the addressBookLoaded() signal
     * and destroy the addressBook there.
     */
    void loadAddressBook( AddressBook *addressBook, AddressBookFile *file, const QString &fileName );

};

#endif // ADDRESSBOOKWORKER_H
