# A project which consists of sub-projects:
TEMPLATE = subdirs

# List the sub-directories:
SUBDIRS += lib AddressBook \
    JSONFileFormatPlugin \
    XMLFileFormatPlugin \
    INIFileFormatPlugin

# Build the included sub-projects in the order given (to avoid build fails due to dependencies):
CONFIG += ordered
