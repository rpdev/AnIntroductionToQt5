#include "mainwindow.h"
#include "addressbook.h"
#include "addressbookcontroller.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    // Create a QApplication (this is required for any widget based application):
    QApplication a(argc, argv);

    // Create an address book:
    AddressBook ab;

    // Create an address book controller and pass it the address book to use:
    AddressBookController controller( &ab );

    // Create the main window and pass it the controller to use:
    MainWindow w( &controller );

    // Show the main window:
    w.show();

    // Start the application's event look:
    return a.exec();
}
