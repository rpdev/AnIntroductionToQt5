#ifndef ADDRESSBOOKCONTROLLER_H
#define ADDRESSBOOKCONTROLLER_H

#include "addressbook.h"

#include <QObject>

/**
 * @brief The AddressBookController class
 *
 * This class is used as a "man in the middle" between our data model (the AddressBook class)
 * and the UI. While for a smaller example like ours this is nothing considered mandatory, it
 * helps if we want to do bigger changes in the future (e.g. implement alternative UIs on top
 * of our business logic) ;). If you opt for to add a controller in your application, than
 * move any code that is not directly connected to showing anything (presentation layer) to the
 * user into it.
 */
class AddressBookController : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Constructor
     *
     * This will create the controller. Note the use of the Dependency Injection (DI) pattern:
     * Any depencencies that this class requires are passed in via the constructor. The DI
     * pattern helps you breaking your code into more independent junks which in turn helps you
     * maintaining and especially testing the code.
     *
     * In this case, the controller requires an address book as a dependency.
     */
    explicit AddressBookController(AddressBook *addressBook, QObject *parent = 0);

    /**
     * @brief Create a new address book entry
     *
     * This method will just call the address book's createEntry method and prepare the
     * created item a bit (i.e. set a default "name"). It is used
     * to bridge the main window to the address book.
     */
    AddressBookEntry *createEntry();

    /**
     * @brief Delete an address book entry
     *
     * This is used as a bridge between the main window and the actual address book.
     */
    bool deleteEntry( AddressBookEntry *entry );

private:

    /**
     * @brief The address book used by the controller
     */
    AddressBook *m_addressBook;

};

#endif // ADDRESSBOOKCONTROLLER_H
