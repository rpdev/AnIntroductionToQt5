#ifndef ADDRESSBOOKFILEFACTORY_H
#define ADDRESSBOOKFILEFACTORY_H

#include "addressbookfile.h"

#include <QtPlugin>

/**
 * @brief The AddressBookFileFactory class
 *
 * This class is the entry point into plugins. A file format plugin must export a class
 * implementing this interface. That instance is then used as a factory to create instances
 * of AddressBookFile, which is used for the actual saving and storing of address books.
 */
class AddressBookFileFactory {

public:

    virtual ~AddressBookFileFactory() {}

    /**
     * @brief Creates a new AddressBookFile instance
     */
    virtual AddressBookFile *createAddressBookFile() const = 0;

    /**
     * @brief The human readable name of the file format created by this factory
     */
    virtual QString name() const = 0;

    /**
     * @brief The file name extension (without leading '.') used by this file format
     * @return
     */
    virtual QString fileNameExtension() const = 0;
};

/*
 * Associate the interface with a unique ID
 */
Q_DECLARE_INTERFACE( AddressBookFileFactory, 
                     "net.rpdev.AddressBookFileFactory" )

#endif // ADDRESSBOOKFILEFACTORY_H

