#ifndef ADDRESSBOOKENTRY_H
#define ADDRESSBOOKENTRY_H

#include "addressbooklibrary.h"

#include <QDate>
#include <QObject>

/**
 * @brief The AddressBookEntry class
 *
 * This class represents a single entry in our address book. It defines some standard properties
 * typical for contact data (name, address, phone numbers, ...).
 *
 * The class derives from QObject, this allows us to make it children of the AddressBook class which
 * - upon destruction - will dispose any entries it owns automatically, so we do not have to
 * implement this on our own.
 */
class ADDRESSBOOKLIBRARY_EXPORT AddressBookEntry : public QObject
{
    Q_OBJECT
    /**
      * Via Q_PROPERTY we make the properties of the class available via Qt's Meta Object System.
      * For now, we don't require this, but it is good practise to make your properties
      * available via this anyhow.
      */
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(QStringList phoneNumbers READ phoneNumbers WRITE setPhoneNumbers NOTIFY phoneNumbersChanged)
    Q_PROPERTY(QDate birthday READ birthday WRITE setBirthday NOTIFY birthdayChanged)
public:

    /**
     * @brief Constructor
     *
     * This is the constructor. As we derive from QObject and want to make use of the parent-child
     * relationship, we define a parent parameter. The parent will own the created object and
     * dispose it as soon as itself gets destroyed.
     */
    explicit AddressBookEntry(QObject *parent = 0);

    /**
     * @brief The name property
     *
     * This are the getters and setters for the name property
     */
    QString name() const;
    void setName(const QString &name);

    /**
     * @brief The address property
     */
    QString address() const;
    void setAddress(const QString &address);

    /**
     * @brief the phoneNumbers property
     */
    QStringList phoneNumbers() const;
    void setPhoneNumbers(const QStringList &phoneNumbers);

    /**
     * @brief The birthday property
     */
    QDate birthday() const;
    void setBirthday(const QDate &birthday);

signals:

    /**
     * @brief These signals are emitted when the corresponding properties change
     */
    void nameChanged();
    void addressChanged();
    void phoneNumbersChanged();
    void birthdayChanged();

public slots:

private:

    /**
     * @brief The property member variables
     */
    QString m_name;
    QString m_address;
    QStringList m_phoneNumbers;
    QDate m_birthday;
};

#endif // ADDRESSBOOKENTRY_H
