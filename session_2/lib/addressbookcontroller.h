#ifndef ADDRESSBOOKCONTROLLER_H
#define ADDRESSBOOKCONTROLLER_H

#include "addressbook.h"
#include "addressbooklibrary.h"
#include "pluginmanager.h"

#include <QObject>

/**
 * @brief The AddressBookController class
 *
 * This class is used as a "man in the middle" between our data model (the AddressBook class)
 * and the UI. While for a smaller example like ours this is nothing considered mandatory, it
 * helps if we want to do bigger changes in the future (e.g. implement alternative UIs on top
 * of our business logic) ;). If you opt for to add a controller in your application, than
 * move any code that is not directly connected to showing anything (presentation layer) to the
 * user into it.
 */
class ADDRESSBOOKLIBRARY_EXPORT AddressBookController : public QObject
{
    Q_OBJECT
public:

    /**
     * @brief Constructor
     *
     * This will create the controller. Note the use of the Dependency Injection (DI) pattern:
     * Any depencencies that this class requires are passed in via the constructor. The DI
     * pattern helps you breaking your code into more independent junks which in turn helps you
     * maintaining and especially testing the code.
     *
     * In this case, the controller requires an address book as a dependency.
     */
    explicit AddressBookController(AddressBook *addressBook, 
                                   PluginManager *pluginManager,
                                   QObject *parent = 0);

    /**
     * @brief Create a new address book entry
     *
     * This method will just call the address book's createEntry method and prepare the
     * created item a bit (i.e. set a default "name"). It is used
     * to bridge the main window to the address book.
     */
    AddressBookEntry *createEntry();

    /**
     * @brief Delete an address book entry
     *
     * This is used as a bridge between the main window and the actual address book.
     */
    bool deleteEntry( AddressBookEntry *entry );

    /**
     * @brief Returns the file name filter string
     *
     * This returns a string of the form 'Format Name 1 (*.ext1)::Format Name 2 (*.ext2);;...'.
     * The string is created from the meta information of the loaded file format
     * plugins.
     */
    QString fileNameExtensions() const;    

    /**
     * @brief Saves the address book to the given @p fileName
     */
    bool saveAddressBook( const QString &fileName );

    /**
     * @brief Loads the address book from the given @p fileName
     */
    bool loadAddressBook( const QString &fileName );

    /**
     * @brief Returns the address book used by the controller
     */
    AddressBook *addressBook() const;
    
private:
    
    /**
     * @brief The address book used by the controller
     */
    AddressBook *m_addressBook;
    
    /**
     * @brief The plugin manager to be used by the controller
     */
    PluginManager *m_pluginManager;

};

#endif // ADDRESSBOOKCONTROLLER_H
