#include "addressbookcontroller.h"

#include <QFileInfo>

AddressBookController::AddressBookController(AddressBook *addressBook, PluginManager *pluginManager, QObject *parent) :
    QObject(parent),
    m_addressBook( addressBook ),
    m_pluginManager( pluginManager )
{
    /*
     * Via Q_ASSERT we ensure that we got valid pointers. It is good practise to make as much
     * use of these asserts as possible to find issues early in development.
     */
    Q_ASSERT( addressBook != nullptr );
    Q_ASSERT( pluginManager != nullptr );
}

/*
 * This causes a new address book entry to be created. It just calls the address book's
 * createEntry() method. If that once suceeds and returns a valid entry, we prepare it a bit and
 * return it as it is.
 */
AddressBookEntry* AddressBookController::createEntry()
{
    auto result = m_addressBook->createEntry();
    if ( result ) {
        result->setName( tr( "New entry" ) );
    }
    return result;
}

/*
 * Same here: We just delegate the deletion request to the address book and
 * return the status code.
 */
bool AddressBookController::deleteEntry(AddressBookEntry *entry)
{
    return m_addressBook->deleteEntry( entry );
}

/*
 * This method creates a file name extension string useable with Qt's
 * QFileDialog. It creates the string using the meta information from the
 * loaded plugins.
 */
QString AddressBookController::fileNameExtensions() const
{
    QStringList extensions;
    for ( auto factory : m_pluginManager->fileFactories() ) {
        QString extension = QString( "%1 ( *.%2)" )
                .arg( factory->name() )
                .arg( factory->fileNameExtension() );
        extensions << extension;
    }
    return extensions.join( ";;" );
}

bool AddressBookController::saveAddressBook(const QString &fileName)
{
    // Get the file name extension:
    QFileInfo fi( fileName );
    QString fileNameExtension = fi.suffix();

    // Search for the factory which handles this file extension:
    for ( auto factory : m_pluginManager->fileFactories() ) {
        if ( factory->fileNameExtension() == fileNameExtension ) {

            // Create a file format and use it to safe the current address book:
            auto file = factory->createAddressBookFile();
            if ( file != nullptr ) {
                bool result = file->saveAddressBook( m_addressBook, fileName );
                delete file;
                return result;
            }
        }
    }
    return false;
}

bool AddressBookController::loadAddressBook(const QString &fileName)
{
    QFileInfo fi( fileName );
    QString fileNameExtension = fi.suffix();

    for ( auto factory : m_pluginManager->fileFactories() ) {
        if ( factory->fileNameExtension() == fileNameExtension ) {
            auto file = factory->createAddressBookFile();
            if ( file != nullptr ) {
                m_addressBook->clear();
                bool result = file->loadAddressBook( m_addressBook, fileName );
                delete file;
                return result;
            }
        }
    }
    return false;
}

AddressBook *AddressBookController::addressBook() const
{
    return m_addressBook;
}

