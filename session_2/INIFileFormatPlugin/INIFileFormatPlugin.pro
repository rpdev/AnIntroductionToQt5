# No GUI:
QT       -= gui

TARGET = INIFileFormatPlugin
TEMPLATE = lib

# Enable C++11 support, do build as plugin (i.e. do not include version number in file name):
CONFIG += plugin c++11

SOURCES += inifileformatplugin.cpp \
    inifileformat.cpp

HEADERS += inifileformatplugin.h \
    inifileformat.h

# Put the resulting library in the AddressBookPlugins directory:
DESTDIR = ../bin/AddressBookPlugins

# Set up library search paths and link against AddressBookLibrary:
LIBS += -L../bin  -lAddressBookLibrary

# Include headers from the AddressBook library (lib dir):
INCLUDEPATH += ../lib
