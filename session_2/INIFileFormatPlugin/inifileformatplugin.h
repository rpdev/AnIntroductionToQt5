#ifndef INIFILEFORMATPLUGIN_H
#define INIFILEFORMATPLUGIN_H

#include "addressbookfilefactory.h"

#include <QObject>

/**
 * @brief The INIFileFormatPlugin class
 *
 * This class is created exactly once. It acts as a factory, creating instances of
 * INIFileFormat.
 */
class INIFileFormatPlugin : public QObject, AddressBookFileFactory
{
    Q_OBJECT
    Q_INTERFACES( AddressBookFileFactory )

    // The plugin ID, must be unique:
    Q_PLUGIN_METADATA( IID "net.rpdev.AddressBook.INIFileFormatPlugin" )
public:
    explicit INIFileFormatPlugin( QObject *parent = 0 );

    // AddressBookFileFactory interface
    AddressBookFile *createAddressBookFile() const override;
    QString name() const override;
    QString fileNameExtension() const override;
};

#endif // INIFILEFORMATPLUGIN_H
