#include "xmlfileformatplugin.h"

#include "xmlfileformat.h"

XMLFileFormatPlugin::XMLFileFormatPlugin(QObject *parent) :
    QObject( parent )
{
}

AddressBookFile *XMLFileFormatPlugin::createAddressBookFile() const
{
    return new XMLFileFormat();
}

QString XMLFileFormatPlugin::name() const
{
    return tr( "XML" );
}

QString XMLFileFormatPlugin::fileNameExtension() const
{
    return "xml";
}
