#ifndef XMLFILEFORMAT_H
#define XMLFILEFORMAT_H

#include "addressbookfile.h"

#include <QObject>

/**
 * @brief The XMLFileFormat class
 *
 * Implements support for the XML file format.
 */
class XMLFileFormat : public QObject, public AddressBookFile
{
    Q_OBJECT

    // This class implements the AddressBookFile interface
    Q_INTERFACES( AddressBookFile )
public:
    explicit XMLFileFormat(QObject *parent = 0);

    // AddressBookFile interface
    bool saveAddressBook(const AddressBook *addressBook, const QString &fileName) const override;
    bool loadAddressBook(AddressBook *addressBook, const QString &fileName) const override;
};

#endif // XMLFILEFORMAT_H
