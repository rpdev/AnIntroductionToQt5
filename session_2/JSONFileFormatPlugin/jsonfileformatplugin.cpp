#include "jsonfileformatplugin.h"

#include "jsonfileformat.h"

JSONFileFormatPlugin::JSONFileFormatPlugin(QObject *parent) :
  QObject( parent )
{
}

AddressBookFile *JSONFileFormatPlugin::createAddressBookFile() const
{
  return new JSONFileFormat();
}

QString JSONFileFormatPlugin::name() const
{
  return tr( "JSON" );
}

QString JSONFileFormatPlugin::fileNameExtension() const
{
  return "json";
}
