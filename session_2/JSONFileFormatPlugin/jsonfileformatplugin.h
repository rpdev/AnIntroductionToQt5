#ifndef JSONFILEFORMATPLUGIN_H
#define JSONFILEFORMATPLUGIN_H

#include "addressbookfilefactory.h"

#include <QObject>

/**
 * @brief The JSONFileFormatPlugin class
 *
 * This class is used as factory which creates instances of JSONFileFormat.
 */
class JSONFileFormatPlugin : public QObject, public AddressBookFileFactory
{
    Q_OBJECT

    // This class implements the AddressBookFileFactory interface:
    Q_INTERFACES( AddressBookFileFactory )

    // The unique ID of the plugin:
    Q_PLUGIN_METADATA( IID "net.rpdev.AddressBook.JSONFileFormatPlugin" )
public:
    JSONFileFormatPlugin( QObject *parent = 0 );

    // AddressBookFileFactory interface
    AddressBookFile *createAddressBookFile() const override;
    QString name() const override;
    QString fileNameExtension() const override;
};

#endif // JSONFILEFORMATPLUGIN_H
